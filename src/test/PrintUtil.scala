package test

object PrintUtil {
   implicit class Print(a: Any) {
    def print : Unit = {
      println(a);
    }
  }
}