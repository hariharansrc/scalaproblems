package test

object PrintUtil2 {
   implicit class Print(a: Any) {
    def \> : Unit = {
      println(a);
    }
  }
}