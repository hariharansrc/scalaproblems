package euler

object FindLargeSubstring extends App{
    def lengthOfLongestSubstring(s: String): Int = {
       val x= for(
            i <- (0 to s.length);
            j <- (0 to s.length)
        )yield s.slice(i,j)
        println(x.size)
        val y=x
        .filter(c=>c.distinct.size==c.size)
        .filter(!_.isEmpty)
        .map(c=>(c,c.size))
        .maxBy(_._2)
        y._2
    }
    println(lengthOfLongestSubstring(""))
}