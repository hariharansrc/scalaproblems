package euler
import euler.Util._
object Fold {

  val nil = Util.Nil

  def main(args: Array[String]): Unit = {

    case class SList[A](head: A, tail: SList[A], empty: Boolean = false)
    val nil = SList[Nothing](_, _, true)

    def printList(ints: List[Int]): Unit = ints match {
      case h :- t => {
        println(h)
        printList(t)
      }
      case _ =>
    }
  }
}

object Util {

  trait List[+A] {
    def head: A

    def tail: List[A]

    def empty: Boolean

  }

  case object Nil extends List[Nothing] {
    override def head: Nothing = ???

    override def tail: List[Nothing] = ???

    override def empty: Boolean = true
  }

  case class :-[A](h: A, t: List[A]) extends List[A] {
    override def head: A = h

    override def tail: List[A] = t

    override def empty: Boolean = false


  }

}