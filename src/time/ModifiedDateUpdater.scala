package time
import java.nio.file.{Files, Paths}

object ModifiedDateUpdater {
  val LOCATION = "/home/invc/test"
  def createFiles() = {
    val basePath = Stream.continually(Paths.get(LOCATION))
    val txtFiles = basePath.flatMap { x => ('A' to '[').map { y => x.resolve(y.toString() + ".txt") } }
      .takeWhile { x => !x.toString().endsWith("[.txt") }
    txtFiles.foreach { x => println(x); Files.createFile(x) }
  }
  def modifyTime() = {
    
  Paths.get(LOCATION).toFile().listFiles().map { x => x.setLastModified(System.currentTimeMillis()) }
  }
  def main(args: Array[String]): Unit = {
   // createFiles()
    modifyTime()
  }
}