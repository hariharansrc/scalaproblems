package hackrank

import hackrank.TripletComparison._

object CompareTriplets {

  def main(args: Array[String]): Unit = {
    val solve = new TripletComparison
    solve.solve()
  }
}
trait Solve[IN, OUT] {
  def takeInput(): IN;

  def validateInput(input: IN)

  def solve();
}
object TripletComparison {
  type Ints = Iterable[Int]

  def compareTo(a: Int, b: Int): Int = {
    if (a > b)
      return 1
    else if (a == b)
      return 0
    else
      return -1
  }
}
class TripletComparison extends Solve[(Ints, Ints), Ints] {
  override def takeInput(): (Ints, Ints) = {
    val a = io.StdIn.readLine().split("""\s+""").map(_.toInt).toIterable
    val b = io.StdIn.readLine().split("""\s+""").map(_.toInt).toIterable
    (a, b)
  }

  override def validateInput(input: (Ints, Ints)): Unit = ???


  override def solve(): Unit = {
    val (a, b) = takeInput()
    val score = for {(i, j) <- (a zip b)} yield {
      val ca = compareTo(i, j)
      val cb = compareTo(j, i)
      (ca, cb)
    }
    val (suma, sumb) = score.unzip

    println(suma.filter(_ >= 0).sum + " " + sumb.filter(_ >= 0).sum)

  }
}

