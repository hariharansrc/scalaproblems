package hackrank;

object ArraySum {

  def main(args: Array[String]): Unit = {
    val solve = new ArraySum
    solve.solve()
  }
}
trait Solve[IN, OUT] {
  def takeInput(): IN;

  def validateInput(input: IN)

  def solve();
}
//Input format ex
// 3
// 1 2 3
class ArraySum extends Solve[(Int, Iterable[Int]), Int] {
  def takeInput() = {
    val n = io.StdIn.readInt()
    val numbers = io.StdIn.readLine().split("""\s+""").map(_.toInt)
    if(n!=numbers.size)
      throw new IllegalArgumentException("Invalid input")
    (n, numbers.toIterable)
  }


  override def solve(): Unit = {
    println(takeInput()._2.sum)
  }

  override def validateInput(input: (Int, Iterable[Int])): Unit = ???
}