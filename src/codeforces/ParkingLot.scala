package codeforces

import java.util.Scanner
object ParkingLot {
  def fact(n: Int): Int = (1 to n).foldLeft(1)(_ * _)
  def choose(n: Int, k: Int): Int = fact(n) / (fact(n - k) * fact(k))
  def main(args: Array[String]): Unit = {
    val sc = new Scanner(System.in);
    val pl = new ParkingLot(sc.nextInt())
    println(pl.noOfArrangements)
  }
  class ParkingLot(n: Int) {
    val nos = 2 * n - 2
    def noOfArrangements: Int = fact(nos-n)*fact(n)*choose(4,n)
  }
}