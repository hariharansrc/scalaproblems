package uva

object CollatzConjecture {
  def collatzLength(initVal: Int): Int = {
    def ca(pair: (Int, Int)): (Int, Int) = pair match {
      case (num, len) if num == 1 => (num, len + 1)
      case (num, len)             => ca((next(num), len + 1))
    }
    ca((initVal, 0))._2
  }
  def odd(n: Int): Int = 3 * n + 1
  def even(n: Int): Int = n / 2
  def next(n: Int): Int = n match {
    case n if n % 2 == 0 => even(n)
    case _               => odd(n)
  }
  def main(args: Array[String]): Unit = {
    println((10 to 100).toStream.par.map(collatzLength(_)).max)

  }
}